## dqAODV For VANET

#### Profil :
|||
| --- | --- |
| Nama | Alvin Mudhoffar |
| NRP | 05111540000062 |
| Kelas | Jaringan Nirkabel |


## Tujuan Modifikasi

- Mengurangi kegagalan saat RREP mencari rute kembali ke node sumber
- Untuk memerikan hasil throughput yang lebih baik daripada AODV biasa

## Cara kerja 

- Melakukan modifikasi pada tabel routing agar menyimpan seluruh informasi (tidak di Drop) pada saat broadcasting RREQ
